package com.livecode;

import com.sun.source.tree.SynchronizedTree;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<String> stringList = initStringList();

        // Sort stringList by using Collection.sort
        System.out.println("== Collection.sort");
        Collections.sort(stringList);
        System.out.println(stringList);
        System.out.println("");

        //Descending
        Collections.sort(stringList, Collections.reverseOrder());
        System.out.println(stringList);
        System.out.println("");

        // Store sorted list into new variable
        System.out.println("== Stream sort ==");
        List<String> sortedStringList = stringList.stream().sorted().collect(
                Collectors.toList());
        System.out.println("Sorted list: " + sortedStringList);
        System.out.println("Original list: " + stringList);
        System.out.println("");


        List<LiveCodeMember> liveCodeMemberList = initObjectList();

        //Sort ArrayList based on object field
        System.out.println("== Sort by id field ==");
        Comparator<LiveCodeMember> idComparator = new Comparator<LiveCodeMember>() {
            @Override
            public int compare(LiveCodeMember o1, LiveCodeMember o2) {
                if (o1.getId() > o2.getId()) {
                    return 1;
                } else if (o1.getId() < o2.getId()) {
                    return -1;
                } else {
                    return 0;
                }

            }
        };
        liveCodeMemberList.sort(idComparator);
        System.out.println(liveCodeMemberList);
        System.out.println("");

        System.out.println("== Sort by age field ==");
        // Lambda function coding style
        Comparator<LiveCodeMember> ageComparator = (a, b) -> Integer
                .compare(b.getAge(), a.getAge());
        liveCodeMemberList.sort(ageComparator);
        System.out.println(liveCodeMemberList);
        System.out.println("");

        // Ascending
//        ageComparator = (a,b) -> Integer.compare(a.getAge(),b.getAge());
        ageComparator = Comparator.comparingInt(LiveCodeMember::getAge);
        liveCodeMemberList.sort(ageComparator);
        System.out.println(liveCodeMemberList);
        System.out.println("");

    }

    //Init String List
    public static List<String> initStringList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("Welcome");
        stringList.add("To");
        stringList.add("Live Code");
        System.out.println("== Original ==");
        System.out.println(stringList);
        System.out.println("");
        return stringList;
    }

    //Init LiveCodeMember List
    public static List<LiveCodeMember> initObjectList() {
        List<LiveCodeMember> list = new ArrayList<>();
        list.add(new LiveCodeMember(1, "Live Code Member 1", 20));
        list.add(new LiveCodeMember(3, "Live Code Member 3", 23));
        list.add(new LiveCodeMember(2, "Live Code Member 2", 18));
        System.out.println("== Original ==");
        System.out.println(list);
        System.out.println("");
        return list;
    }
}
